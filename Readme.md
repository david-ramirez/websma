# WebSMA

Proyecto desarrollado para la asignatura __Software para Matemática Aplicada__.

Se trata de una página web de ayuda a estudiantes de bachiller. La página contiene tests de diferentes temas, cada uno de ellos con 5 ejercicios de 3 opciones. Una vez contestada una pregunta, se corregirá indicando si la respuesta es o no correcta y al final se mostrará la puntuación total obtenida.

Los diferentes ejercicios han sido resueltos con el software [wxMaxima](http://andrejv.github.io/wxmaxima/) previo a la presentación en las diferentes páginas.

Los temas incluidos, extraídos del temario de matemáticas de 2º de bachiller de la modalidad de ciencias, es el siguiente:

1.  Números complejos.
2.  Funciones.
3.  Funciones. Límites y continuidad.
4.  Sistemas de ecuaciones lineales.
5.  Derivadas.
6.  Integrales. Integrales definidas e indefinidas.
7.  Matrices. Operaciones con matrices, cálculo de rango y determinantes.
8.  Ejercicios de selectividad.

## Funcionalidad desarrollada

La lógica desarrollada para los test es la siguiente:
* __Corrección de ejercicios__. Una vez que el usuario pulsa en el botón de validación correspondiente al ejercicio, se comprueba la respuesta y se indica si es correcta o no.
* __Informe global__. A medida que el usuario responte las preguntas, se actualiza el informe del test situado al final de la página.
* __Cronómetro__. Se contabiliza el tiempo necesario para la resolución del test desde el momento que carga la página. Es posible pausar el cronómetro, ocultando a su vez los ejercicios. Estos se vuelven a mostrar al continuar el test.

## Lenguajes utilizados

* __HTML__
* __Javascript__
* __CSS__

## Navegadores soportados

La funcionalidad Javascript desarrollada depende del acceso DOM del documento y puede fallar dependiendo del navegador utilizado.

Ha sido comprobado el funcionamiento correcto de la página en los siguientes navegadores:

* Mozilla Firefox versión __36__.
* Google Chrome versión __41__.

## Notas de desarrollo

La página desarrollada se ejecuta directamente en navegador sin necesidad de un servidor. Además, al carecer de un servidor los documentos HTML son los encargados de almacenar toda la información relevante a cada test, incluyendo las respuestas correctas a los ejercicios.
