
//Variables globales
var pregRespondidas;
var pregTotales;
var pregCorrectas;

//para el contador
var reloj;
var contador;

/**
*	Funcion encargada de inicializar la lógica del test una vez que se carga la página
**/
function inicializar(){
	pregCorrectas = 0;
	pregRespondidas = 0;
	pregTotales = 0;
	
	//Obtener el numero de preguntas que hay
	//Es lo mismo que el numero de hijos que son formularios dentro del article de id = test
	
	var test = document.getElementById("test");
	
	for(var i = 0; i < test.children.length; i++)
	{
		var nombre = test.children[i].localName;
		if(nombre == "form")
		{
			pregTotales++;
		}
	}
	
	//Actualizar barra de resultados
	actualizarResultado();
	
	//iniciar el contador
	contador = 0;
	reloj = setInterval(contar, 1000);
	
	//Asociar eventos a los botones
	document.getElementById("pausa").onclick = pausa;
	document.getElementById("continua").onclick = continua;
	
	//Establecer si son accionables
	document.getElementById("pausa").disabled=false;
	document.getElementById("continua").disabled=true;
}

window.onload = inicializar;

/**
*	Función que dado un formulario, comprueba si la respuesta es correcta
**/
function comprobarRespuesta(formId)
{
	//Recoger la opcion marcada
	var opcion = document.forms[formId]["opcion"];
	
	//Elemento con la respuesta a la corrección
	var respuesta = document.createElement("div");
	
	//Comprobar que una de las opciones está seleccionada
	if(opcion.value == "" || opcion.value == null)
	{
		alert("Responde la pregunta para poder validarla");
		return;//salimos de esta funcion
	}
	else if(opcion.value == "correcto")
	{
		//Si la respuesta es correcta, actualizamos contadores 
		pregRespondidas++;
		pregCorrectas++;
		//preparar la respuesta
		respuesta.id="acierto";
		respuesta.innerHTML= "<h2>La respuesta es correcta</h2>";
	}
	else
	{
		//Si la respuesta es incorrecta, actualizamos contadores
		pregRespondidas++;
		//Preparar la respuesta
		respuesta.id="error";
		respuesta.innerHTML= "<h2>La respuesta no es correcta</h2>";
	}
	//introducir el mensaje
	document.forms[formId].appendChild(respuesta);
	
	//inhabilitar el cambio de opción y el boton
	document.forms[formId]["opt1"].disabled=true;
	document.forms[formId]["opt2"].disabled=true;
	document.forms[formId]["opt3"].disabled=true;
	document.forms[formId]["comprobar"].disabled=true;
	
	actualizarResultado();
}

/**
*	Funcion que actualiza la seccion del test con los resultados
**/
function actualizarResultado()
{
	//Elemento en el que escribir
	var resultado = document.getElementById("resultado");
	
	//Preparamos el mensaje
	 var res = "Respuestas correctas: " + pregCorrectas + "<br> Preguntas respondidas: " + pregRespondidas + "<br> Preguntas totales: "  + pregTotales;
	
	//Comprobamos si ha terminado
	if(pregRespondidas == pregTotales)
	{
		res += "<h2> Has terminado el test</h2>";
		
		//Detener el contador y deshabilitar sus botones
		clearInterval(reloj);
		document.getElementById("pausa").disabled=true;
		document.getElementById("continua").disabled=true;
	}
	
	//Escribimos el mensaje
	resultado.innerHTML = res;
}


//Funciones para el contador
/**
*	Se ejecuta cada segundo y actualiza el contador mostrado
**/
function contar(){
	contador++;
	
	seg = contador%60;
	min = Math.floor(contador/60);
	
	var capa = document.getElementById("contador");
	capa.innerHTML = min + ":" + seg;
}

/**
*	Gestiona la pausa del contador
**/
function pausa(){
	//Detener reloj
	clearInterval(reloj);
	//Cambiar los botones habilitados
	document.getElementById("pausa").disabled=true;
	document.getElementById("continua").disabled=false;
	
	//Ocultar los enunciados y opciones de los ejercicios
	
	var test = document.getElementById("test");
	
	for(var i = 0; i < test.children.length; i++)
	{
		var nombre = test.children[i].localName;
		if(nombre == "form")
		{
			test.children[i].children[0].children["contenido"].hidden = true;
		}
	}
}

/**
*	Gestiona la reanudacion del contador
**/
function continua(){
	//Reanudar el reloj
	reloj = setInterval(contar, 1000);
	//Cambiar los botones habilitados
	document.getElementById("pausa").disabled=false;
	document.getElementById("continua").disabled=true;
	
	//Mostrar los enunciados y opciones de los ejercicios
	var test = document.getElementById("test");
	
	for(var i = 0; i < test.children.length; i++)
	{
		var nombre = test.children[i].localName;
		if(nombre == "form")
		{
			test.children[i].children[0].children["contenido"].hidden = false;
		}
	}
}